#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>

#define NOT_FOUND (-1)
#define PATH_MAX 200
#define ARG_MAX 16

typedef enum { false, true } bool;

int num_procs = 0;

void child_exit(int sig) {
    pid_t p;
    int status;
    for(int i = 0; i < num_procs; i++) {
        p = waitpid(-1, &status, WNOHANG);
        if (WIFEXITED(status)) {
            num_procs--;
        }
    }
}

int getChmod(const char *path);
bool endsWithAmp(const char *str);
bool doBuiltIns(char *cmd, char *options[ARG_MAX]);

// https://stackoverflow.com/questions/12252103/

char cwd[PATH_MAX];

int main(int argc, char** argv, char *envp[]) {
    signal(SIGCHLD, child_exit);

    char *args[ARG_MAX];
    memset(&args[0], 0, sizeof(args));
    char *PATH = getenv("PATH");
    char *path;
    path = (char *) malloc(200);

    char *line = NULL;
    while (true) {
        memset(&args[0], 0, sizeof(args));
        printf("$ ");
        size_t size;
        if (getline(&line, &size, stdin) == -1) {
            printf("No line\n");
        } else {
            bool background = false;
            int count = 0;
            char *start = line;
            char prev = 0;
            while (*line != 0 && count < ARG_MAX) {
                char ch = *line;
                if (ch == '&') {
                    background = true;
                    line++;
                    continue;
                }
                if (ch == '"') {
                    line ++;
                    args[count++] = line;
                    while (*line != '"') {
                        line ++;
                    }
                    *line = 0;
                    line ++;
                    continue;
                }

                if (ch == '\'') {
                    line ++;
                    args[count++] = line;
                    while (*line != '\'') {
                        line ++;
                    }
                    *line = 0;
                    line ++;
                    continue;
                }

                if (ch == 32 || ch == 10 || ch == 13) {
                    *line = 0;
                    if (prev != 0) {
                        args[count++] = start;
                    }
                    start = line + 1;
                }
                prev = *line;
                line++;
            }
            
            // for (int i = 0; i < count; i++) {
            //     printf("args: %s\n", args[i]);
            // }

            int status;
            bool done = doBuiltIns(args[0], args);
            if (done) continue;
            int new_proc = fork();
            int piped = 0;
            
            if (new_proc == 0) {
                int i = 0;
                while (args[i] != '\0') {
                    if (strcmp(args[i], "|") == 0) {
                        int pipe_fd[2];
                        pipe(pipe_fd);
                        args[i++] = '\0';
                        piped = fork();
                        if (piped == 0) {
                            close(STDOUT_FILENO);
                            dup(pipe_fd[1]);
                            close(pipe_fd[0]);
                            close(pipe_fd[1]);
                            break;
                        } else {
                            int j = i;
                            i = 0;
                            while (j < count) {
                                args[i++] = args[j++];
                            }
                            args[i++] = '\0';
                            close(STDIN_FILENO);
                            dup(pipe_fd[0]);
                            close(pipe_fd[0]);
                            close(pipe_fd[1]);
                            i = 0;
                            continue;
                        }
                    }
                    else if (strcmp(args[i], "<") == 0) {
                        close(STDIN_FILENO);
                        open(args[i + 1], O_RDONLY);
                        args[i] = '\0';
                    } else if (strcmp(args[i], ">") == 0) {
                        close(STDOUT_FILENO);
                        getcwd(cwd, sizeof(cwd));
                        int chmod = getChmod(cwd);
                        open(args[i + 1], O_WRONLY|O_CREAT, chmod);
                        args[i] = '\0';
                    }
                    i++;
                }

                char *slashPos = strchr(args[0], '/');
                if (slashPos) {
                    execve(args[0], args, envp);
                    if (errno == ENOENT) {
                        printf("no command found: %s\n", args[0]);
                    }
                } else {
                    strcpy(path, PATH);
                    char *ptoken;
                    ptoken = strtok(path, ":");
                    char *fullpath;
                    fullpath = (char *) malloc(50);
                    while (ptoken != NULL) {
                        strcpy(fullpath, ptoken);
                        strcat(fullpath, "/");
                        strcat(fullpath, args[0]);
                        // printf("%s\n",fullpath);
                        execve(fullpath, args, envp);
                        if (errno != ENOENT) break;
                        ptoken = strtok(NULL, ":");
                    }
                    if (errno == ENOENT) {
                        printf("no command found (in path): %s\n", args[0]);
                    }
                }
                if (errno == EACCES) {
                    printf("permission denied\n");
                } else if (errno == ENOEXEC) {
                    printf("exec format error\n");
                }

                exit(0);
            } else {
                if (!background) {
                    waitpid(new_proc, &status, 0);
                } else {
                    num_procs++;
                }
            }
        }
    }
    return 0;
}

bool doBuiltIns(char *cmd, char *options[ARG_MAX]) {
    if (strcmp(cmd, "cd") == 0) {
        int res = chdir(options[1]);
        // printf("cd option: %s\n", options[1]);
        if (res) {
            if (errno == EACCES) {
                printf("permission denied\n");
            } else if (errno == ENOTDIR) {
                printf("component in dir path is not a directory\n");
            } else if (errno == ENOENT) {
                printf("no such file or directory\n");
            }
            else {
                printf("err: %d %d\n", res, errno);
            }
        }
    } else if (strcmp(cmd, "exit") == 0) {
        exit(0);
    } else if (strcmp(cmd, "pwd") == 0) {
        if (getcwd(cwd, sizeof(cwd)) != NULL) {
            printf("%s\n", cwd);
        } else {
            perror("getcwd() error");
        }
    } else if (strcmp(cmd, "version") == 0) {
        printf("edric's shell\n");
    } else return false;
    
    return true;
}

int getChmod(const char *path){
    struct stat ret;

    if (stat(path, &ret) == -1) {
        return -1;
    }

    return (ret.st_mode & S_IRUSR)|(ret.st_mode & S_IWUSR)|(ret.st_mode & S_IXUSR)|/*owner*/
        (ret.st_mode & S_IRGRP)|(ret.st_mode & S_IWGRP)|(ret.st_mode & S_IXGRP)|/*group*/
        (ret.st_mode & S_IROTH)|(ret.st_mode & S_IWOTH)|(ret.st_mode & S_IXOTH);/*other*/
}