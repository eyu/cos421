#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef enum { false, true } bool;

#define PAGE_MASK 0xFFF

char line[256];
unsigned long page_num_mask = ~PAGE_MASK;

void parse(char* line) {
}

//stackoverflow.com/questions/122616
char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  // if(*str == 0)  // All spaces?
  //   return str;

  // // Trim trailing space
  // end = str + strlen(str) - 1;
  // while(end > str && isspace((unsigned char)*end)) end--;

  // // Write new null terminator character
  // end[1] = '\0';

  return str;
}

bool validop(unsigned char ch) {
  if (ch == 'I' || ch == 'S' || ch == 'L' || ch == 'M')
    return true;
  return false;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("need argument\n");
    exit(0);
  }
  char *filename = argv[1];
  FILE* file = fopen(filename, "r");
  
  //getline(&line, &size, stdin)
  while (fgets(line, sizeof(line), file)) {
    char *cursor = line;
    cursor = trimwhitespace(cursor);
    char op = (unsigned char)*cursor;
    if (!validop(op)) continue;
    
    printf("%s", cursor);
    cursor++;
    cursor = trimwhitespace(cursor);
    char *addr = strtok(cursor, ",");
    int size = atoi(trimwhitespace(strtok(NULL, "\n")));
    unsigned long addr1 = strtoul(addr, NULL, 16);
    unsigned long addr2 = addr1 + size;

    unsigned long page1 = addr1 & page_num_mask;
    unsigned long page2 = addr2 & page_num_mask;
    printf("%x %x\n", page1, page2);
  }
}
